#include "GameView.h"
#include <iostream>
using namespace std;
using namespace sf;

int GameView::scrolling=0;


GameView::GameView(int w, int h, int bpp): _width(w), _height(h)
{
    _window = new RenderWindow(sf::VideoMode(w, h, bpp), "Shoot Them Up", sf::Style::Close);
    _font=new Font();
    _font->LoadFromFile("Antique Olive.ttf");

    _window->SetSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    _window->Clear(Color(255,255,255));                           // Nettoie la fenetre avec une couleur (noir par defaut)


    // Si les image ne sont chargés, on les charge
    if ((!_background_image.LoadFromFile("images/background_1.png")) ||(!_menu_image.LoadFromFile("images/background_1.png"))
            || (!_play_button_1_image.LoadFromFile("images/bouton_001.png")) || (!_play_button_2_image.LoadFromFile("images/bouton_002.png"))
            || (!_bomb_image.LoadFromFile("images/bomb1.png") || (!_shield_image.LoadFromFile("images/shield1.png")) || (!_life_image.LoadFromFile("images/Coeur_2.png"))
                || (!_sound_muted_image.LoadFromFile("images/mute.png")) || (!_sound_unmuted_image.LoadFromFile("images/unmute.png"))
                || (!my_image.LoadFromFile("images/my_images.png")))
                || (!_logo_image.LoadFromFile("images/logo.jpeg")))
    {

        _background_sprite = Sprite ();
        _background_sprite2 = Sprite ();

        _bomb_sprite = Sprite();
        _shield_sprite = Sprite();
        _life_sprite = Sprite();
        _explosion_sprite1 = Sprite();
        _explosion_sprite2 = Sprite();
        _explosion_sprite3 = Sprite();
        _explosion_sprite4 = Sprite();
        _sound_muted_sprite = Sprite();

        _logo_sprite=Sprite();

        /// MENU ///
        _menu_sprite = Sprite();
        _play_button_1_sprite = Sprite();
        _play_button_2_sprite = Sprite();
        _score_button_1_sprite = Sprite();
        _score_button_2_sprite = Sprite();
        _quit_button_1_sprite = Sprite();
        _quit_button_2_sprite = Sprite();

        /// ELEMENTS QUI BOUGE ///
        _player_sprite = Sprite();     //player
        _player_sprite_shield = Sprite();
        _missile_sprite = Sprite();     //missile
        _missile_enemy_sprite = Sprite();
        _laser_sprite = Sprite();
        _enemy1_sprite = Sprite();
        _enemy2_sprite = Sprite();
        _enemy3_sprite = Sprite();
    }
    // Initialise les sprites et les redimensionne
    else
    {
        /// BACKGROUND ///
        _background_sprite = Sprite(_background_image);
        _background_sprite.Resize(_width, _height);    // affiche le sprite du fond d'écran
        _background_sprite.SetPosition(0,0);                // position initiale

        _background_sprite2 = Sprite (_background_image);
        _background_sprite2.Resize(_width,_height);    // affiche le sprite du fond d'écran
        _background_sprite2.SetPosition(-SCREEN_WIDTH,0);                // position initiale

        _logo_sprite=Sprite(_logo_image);

        /// BONUS ///
        _bomb_sprite = Sprite (_bomb_image);
        _bomb_sprite.Resize(BONUS_SIZE,BONUS_SIZE);
        _shield_sprite = Sprite (_shield_image);
        _shield_sprite.Resize(BONUS_SIZE,BONUS_SIZE);
        _life_sprite = Sprite(_life_image);
        _life_sprite.Resize(50,50);

        /// EXPLOSION ///
        _explosion_sprite1 = Sprite(my_image);
        _explosion_sprite1.SetSubRect(IntRect(504,238,86+504,75+238));
        _explosion_sprite1.Resize(80,80);
        _explosion_sprite2 = Sprite(my_image);
        _explosion_sprite2.SetSubRect(IntRect(627,200,198+627,172+200));
        _explosion_sprite2.Resize(80,80);
        _explosion_sprite3 = Sprite(my_image);
        _explosion_sprite3.SetSubRect(IntRect(874,192,220+874,197+192));
        _explosion_sprite3.Resize(80,80);
        _explosion_sprite4 = Sprite(my_image);
        _explosion_sprite4.SetSubRect(IntRect(42,397,214+42,211+397));
        _explosion_sprite4.Resize(80,80);

        /// SON ///
        _sound_muted_sprite = Sprite(_sound_unmuted_image);
        _sound_muted_sprite.Resize(30, 30);
        _sound_muted_sprite.SetPosition(SCREEN_WIDTH-50,50);

        /// VAISSEAUX ///
        _player_sprite = Sprite(my_image);
        _player_sprite.SetSubRect(IntRect(14,5,214+14,141+5));
        _player_sprite.Resize( PLAYER_WIDTH, PLAYER_HEIGHT);
        _player_sprite_shield = Sprite(my_image);
        _player_sprite_shield.SetSubRect(IntRect(12,182,12+274,180+182));
        _player_sprite_shield.Resize(PLAYER_WIDTH, PLAYER_HEIGHT);
        _enemy1_sprite = Sprite(my_image);
        _enemy1_sprite.SetSubRect(IntRect(245,13,263+245,140+13));
        _enemy1_sprite.Resize(ENEMY1_WIDTH, ENEMY1_HEIGHT);
        _enemy2_sprite = Sprite(my_image);
        _enemy2_sprite.SetSubRect(IntRect(800,61,57+800,49+61));
        _enemy2_sprite.Resize(ENEMY2_WIDTH, ENEMY2_HEIGHT);
        _enemy3_sprite = Sprite(my_image);
        _enemy3_sprite.SetSubRect(IntRect(556,31,203+556,114+31));
        _enemy3_sprite.Resize( ENEMY3_WIDTH, ENEMY3_HEIGHT);

        /// ARMES ///
        _missile_sprite = Sprite(my_image);
        _missile_sprite.SetSubRect(IntRect(896,42,29+896,13+42));
        _missile_sprite.Resize( WEAPON_WIDTH, WEAPON_HEIGHT);
        _missile_enemy_sprite = Sprite(my_image);
        _missile_enemy_sprite.SetSubRect(IntRect(893,88,34+893,17+88));
        _missile_enemy_sprite.Resize( WEAPON_WIDTH, WEAPON_HEIGHT);
        _laser_sprite = Sprite(my_image);
        _laser_sprite.SetSubRect(IntRect(883,129,64+883,17+129));


        /// MENU ///
        _menu_sprite = Sprite(_menu_image);
        _menu_sprite.Resize( _width, _height);
        _menu_sprite.SetPosition(0,0);
        _play_button_1_sprite = Sprite(_play_button_1_image);
        _play_button_1_sprite.SetPosition(w/2-60,h/2-240);
        _play_button_2_sprite = Sprite(_play_button_2_image);
        _play_button_2_sprite.SetPosition(w/2-60,h/2-240);
        _score_button_1_sprite = Sprite(_play_button_1_image);
        _score_button_1_sprite.SetPosition(w/2-60,h/2-120);
        _score_button_2_sprite = Sprite(_play_button_2_image);
        _score_button_2_sprite.SetPosition(w/2-60,h/2-120);
        _option_button_1_sprite = Sprite(_play_button_1_image);
        _option_button_1_sprite.SetPosition(w/2-60,h/2);
        _option_button_2_sprite = Sprite(_play_button_2_image);
        _option_button_2_sprite.SetPosition(w/2-60,h/2);
        _quit_button_1_sprite = Sprite(_play_button_1_image);
        _quit_button_1_sprite.SetPosition(w/2-60,h/2+120);
        _quit_button_2_sprite = Sprite(_play_button_2_image);
        _quit_button_2_sprite.SetPosition(w/2-60,h/2+120);

        _play_button_1_sprite.Resize(200,120);
        _play_button_2_sprite.Resize(200,120);
        _quit_button_1_sprite.Resize(200,120);
        _score_button_1_sprite.Resize(200,120);
        _quit_button_2_sprite.Resize(200,120);
        _score_button_2_sprite.Resize(200,120);
        _option_button_1_sprite.Resize(200,120);
        _option_button_2_sprite.Resize(200,120);
    }
}


GameView::~GameView()
{
    if(_window != NULL)
        delete _window;
    if(_font != NULL)
        delete _font;
}


void GameView::draw()
{
    /// BACKGROUND ET DEFILEMENT ///
    _window->Draw(_background_sprite);          // Affiche le sprite dans la fenetre
    _window->Draw(_background_sprite2);          // Affiche le sprite dans la fenetre

    scrolling -= 1;
    _background_sprite.SetPosition(scrolling, 0);
    _background_sprite2.SetPosition(scrolling + SCREEN_WIDTH, 0);

    if (scrolling <= -SCREEN_WIDTH)
        scrolling = 0;


    HUD();


    /// VAISSEAU JOUEUR ///
    if(_model->getPlayer()->getShield()>0)
    {
        _player_sprite_shield.SetPosition(_model->getXPlayer(), _model->getYPlayer());
        _window->Draw(_player_sprite_shield);
    }
    else
    {
        _player_sprite.SetPosition(_model->getXPlayer(), _model->getYPlayer());
        _window->Draw(_player_sprite);
    }

    /// ARMES JOUEUR///
    for (auto m:_model->getPlayerMissiles())
    {
        _missile_sprite.SetPosition(m->getX(), m->getY());
        _window->Draw(_missile_sprite);
    }



    const Input &Input = _window->GetInput();
    if (Input.IsKeyDown(sf::Key::F))
    {
         _laser_sprite.Resize(_model->getPlayerLaser()->getW(), _model->getPlayerLaser()->getH());
       _laser_sprite.SetPosition(_model->getPlayerLaser()->getX(), _model->getPlayerLaser()->getY());

        _window->Draw(_laser_sprite);
    }


    /// ENNEMIS ///
    switch (_model->getLevel())
    {
    case 1:
        for (auto e:_model->getEnemies())
        {
            _enemy1_sprite.SetPosition(e->getX(), e->getY());
            _window->Draw(_enemy1_sprite);
        }
        break;
    case 2:
        for (auto e:_model->getEnemies())
        {
            _enemy2_sprite.SetPosition(e->getX(), e->getY());
            _window->Draw(_enemy2_sprite);
        }
        break;
    case 3:
        for (auto e:_model->getEnemies())
        {
            _enemy3_sprite.SetPosition(e->getX(), e->getY());
            _window->Draw(_enemy3_sprite);
        }
        break;
    default:
        for (auto e:_model->getEnemies())
        {
            _enemy3_sprite.SetPosition(e->getX(), e->getY());
            _window->Draw(_enemy3_sprite);
        }
        break;
    }

    /// MISSILES ENNEMIS ///
    for (auto m:_model->getEnemiesMissiles())
    {
        _missile_enemy_sprite.SetPosition(m->getX(), m->getY());
        _window->Draw(_missile_enemy_sprite);
    }


    _explosion_sprite1.Resize(80,80);
    _explosion_sprite2.Resize(80,80);
    _explosion_sprite3.Resize(80,80);
    _explosion_sprite4.Resize(80,80);

    /// DESTRUCTION ENNEMIS MORTS ///
    for (auto d:_model->_destroyed_enemies)
    {
        _explosion_sprite1.SetPosition(d->getX(), d->getY());
        _explosion_sprite2.SetPosition(d->getX(), d->getY());
        _explosion_sprite3.SetPosition(d->getX(), d->getY());
        _explosion_sprite4.SetPosition(d->getX(), d->getY());

        if (_model->explosion.GetElapsedTime() < EXPLOSION_DURATION-0.2)
        {
            _window->Draw(_explosion_sprite1);
        }

        if (_model->explosion.GetElapsedTime() > EXPLOSION_DURATION-0.2 && _model->explosion.GetElapsedTime() < EXPLOSION_DURATION-0.1)
        {
            _window->Draw(_explosion_sprite2);
        }

        if (_model->explosion.GetElapsedTime() > EXPLOSION_DURATION-0.1 && _model->explosion.GetElapsedTime() < EXPLOSION_DURATION)
        {
            _window->Draw(_explosion_sprite3);
        }
    }

    _explosion_sprite1.Resize(20,20);
    _explosion_sprite2.Resize(20,20);
    _explosion_sprite3.Resize(20,20);
    _explosion_sprite4.Resize(20,20);

    /// DESTRUCTION MISSILES ///
    for (auto m:_model->_player_missiles_destroyed)
    {
        _explosion_sprite1.SetPosition(m->getX()-10, m->getY());
        _explosion_sprite2.SetPosition(m->getX()-10, m->getY());
        _explosion_sprite3.SetPosition(m->getX()-10, m->getY());
        _explosion_sprite4.SetPosition(m->getX()-10, m->getY());


        if (_model->explosion.GetElapsedTime() < EXPLOSION_DURATION-0.2)
        {
            _window->Draw(_explosion_sprite1);
        }

        if (_model->explosion.GetElapsedTime() > EXPLOSION_DURATION-0.2 && _model->explosion.GetElapsedTime() < EXPLOSION_DURATION-0.1)
        {
            _window->Draw(_explosion_sprite2);
        }

        if (_model->explosion.GetElapsedTime() > EXPLOSION_DURATION-0.1 && _model->explosion.GetElapsedTime() < EXPLOSION_DURATION)
        {
            _window->Draw(_explosion_sprite3);
        }
    }

    /// BONUS ///
    if(_model->getBonus()->getId()==0)
    {
        _bomb_sprite.SetPosition(_model->getBonus()->getX(),_model->getBonus()->getY());
        _window->Draw(_bomb_sprite);
    }

    if(_model->getBonus()->getId()==1)
    {
        _shield_sprite.SetPosition(_model->getBonus()->getX(),_model->getBonus()->getY());
        _window->Draw(_shield_sprite);
    }
    if(_model->getBonus()->getId()==2)
    {
        _life_sprite.SetPosition(_model->getBonus()->getX(),_model->getBonus()->getY());
        _window->Draw(_life_sprite);
    }

    _window->Display();                         // Affiche la fenetre à l'ecran
}

void clearFile(string file)
{
    ofstream WriteFile(file);    // ouverture du fichier en écriture
    WriteFile << "";             // on écrit dans le fichier un string vide pour tout effacer
    WriteFile.close();
}


bool GameView::treatEvents()
{
    bool result = false;

    // Si fenetre ouverte
    if(_window->IsOpened())
    {
        result = true;
        bool exit = false;
        Event event;


        if (_model->getEnemyKilled() > 15)
        {
            _model->setLevel(_model->getLevel()+1);
            _model->resetLevel();
            _window->Clear();     // Nettoie la fenetre avec une couleur (noir par defaut)
            if(language==0)
                _level_change=String("LEVEL " + _model->changeString(_model->getLevel()));
            else
                _level_change=String("NIVEAU " + _model->changeString(_model->getLevel()));
            _level_change.SetFont(*_font);
            _level_change.SetColor(Color(255, 255, 255));
            _level_change.SetSize(100.f);
            _level_change.SetPosition(SCREEN_WIDTH/3, SCREEN_HEIGHT/2-100);
            _window->Draw(_level_change);
            _window->Display();                         // Affiche la fenetre à l'ecran
            _model->music.Stop();
            Sleep(2);
            //_model->clock.Reset();
            _model->music.Play();
        }

        if (_model->playerLostLife() && !_model->isDead(_model->getPlayer()))
        {
            _window->Clear();
            _model->music.Stop();

            if(language==0)
            {
                _death=String("WASTED");
                _death.SetPosition(SCREEN_WIDTH/3-30, SCREEN_HEIGHT/4);
                _death.SetSize(100.f);
            }

            else
            {
                _death=String("Tu as perdu une vie");
                _death.SetPosition(120, SCREEN_HEIGHT/4);
                _death.SetSize(80.f);
            }

            _death.SetFont(*_font);
            _death.SetColor(Color(255, 255, 255));

            if(language == 0)
            {
                if (_model->getPlayer()->getLife() > 1)
                    _life=String(_model->changeString(_model->getPlayer()->getLife()) + " lives left");
                else
                    _life=String(_model->changeString(_model->getPlayer()->getLife()) + " life left");
                _life.SetPosition(SCREEN_WIDTH/3+30, SCREEN_HEIGHT/2+30);                // positionne le text
            }
            else
            {
                if (_model->getPlayer()->getLife() > 1)
                    _life=String(_model->changeString(_model->getPlayer()->getLife()) + " vies restantes");
                else
                    _life=String(_model->changeString(_model->getPlayer()->getLife()) + " vie restante");
                _life.SetPosition(SCREEN_WIDTH/4+60, SCREEN_HEIGHT/2+30);
            }

            _life.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
            _life.SetColor(Color(255, 255, 255));
            _life.SetSize(50.f);                                     // change la taille de la police

            _window->Draw(_life);
            _window->Draw(_death);
            _window->Display();                         // Affiche la fenetre à l'ecran
            Sleep(2);
            _model->music.Play();
        }

        if (_model->isDead(_model->getPlayer()))
        {
            _model->music.Stop();
            createScore();

            /// REINITIALISER ///
            _window->Clear();                             // Nettoie la fenetre avec une couleur (noir par defaut)
            if(language==0)
                _death=String("YOU ARE DEAD, TRY AGAIN");
            else
                _death=String("TU ES MORT, REESSAIE");

            _death.SetFont(*_font);
            _death.SetColor(Color(255, 255, 255));
            _death.SetSize(75.f);
            _death.SetPosition(SCREEN_WIDTH/50, SCREEN_HEIGHT/2-50);
            _window->Draw(_death);
            _death=String("SCORE "+_model->changeString(_model->getScore()));
            _death.SetFont(*_font);
            _death.SetColor(Color(255, 255, 255));
            _death.SetSize(75.f);
            _death.SetPosition(SCREEN_WIDTH/2-100, SCREEN_HEIGHT/2+40);
            _window->Draw(_death);

            _window->Display();                           // Affiche la fenetre à l'ecran
            Sleep(3);
            _model->getPlayer()->setLife(PLAYER_LIFE);
            _model->resetScore();
           // _model->clock.Reset();

            menu(exit);
            if(!exit)
            {
                _model->music.Play();
                while (treatEvents())
                {
                    _model->nextStep();
                    draw();
                }
            }
            result = false;
        }

        const Input &Input = _window->GetInput();
        bool Z_key_down = Input.IsKeyDown(sf::Key::Z);
        bool Up_key_down = Input.IsKeyDown(sf::Key::Up);
        bool S_key_down = Input.IsKeyDown(sf::Key::S);
        bool Down_key_down = Input.IsKeyDown(sf::Key::Down);
        bool T_key_down = Input.IsKeyDown(sf::Key::T);
        bool Space_key_down = Input.IsKeyDown(sf::Key::Space);
        bool Q_key_down = Input.IsKeyDown(sf::Key::Q);
        bool Left_key_down = Input.IsKeyDown(sf::Key::Left);
        bool D_key_down = Input.IsKeyDown(sf::Key::D);
        bool Right_key_down = Input.IsKeyDown(sf::Key::Right);
        bool F_key_down = Input.IsKeyDown(sf::Key::F);

        // Se deplacer
        if (Z_key_down || Up_key_down)
            _model->moveUp();

        if (S_key_down || Down_key_down)
            _model->moveDown();

        if (Q_key_down || Left_key_down)
            _model->moveLeft();

        if (D_key_down || Right_key_down)
            _model->moveRight();

        // Tirer un missile
        if ((T_key_down || Space_key_down) && !F_key_down)
            _model->shoot();

        // Tirer au laser
        if (F_key_down && !T_key_down)
            _model->laser(true);


        else
            _model->laser(false);




        while (_window->GetEvent(event))
        {
            /// Si on appuie sur echap ou sur la croix, la fenetre se ferme
            if ((event.Type == Event::Closed))
            {
                _window->Close();
                result = false;
            }
            if ((event.Type == Event::KeyPressed) && (event.Key.Code == sf::Key::Escape))
            {
                pause(result);
            }

            // Lancer une bombe
            if (event.Type == Event::KeyPressed && event.Key.Code == sf::Key::B)
                _model->bomb();


            if (event.Type == Event::MouseButtonReleased && event.MouseButton.X >(SCREEN_WIDTH-50) && event.MouseButton.X<(SCREEN_WIDTH-20) &&
                    event.MouseButton.Y>(50) && event.MouseButton.Y<(50+30))
            {
                if (unmute)
                {
                    _sound_muted_sprite = Sprite(_sound_muted_image);
                    _sound_muted_sprite.Resize(25, 25);
                    _sound_muted_sprite.SetPosition(SCREEN_WIDTH-47,52);
                    unmute = !unmute;
                    _model->music.SetVolume(0);
                    _model->player_shoot_sound.SetVolume(0);
                    _model->enemy_death_sound.SetVolume(0);
                }

                else
                {
                    _sound_muted_sprite = Sprite(_sound_unmuted_image);
                    _sound_muted_sprite.Resize(30, 30);
                    _sound_muted_sprite.SetPosition(SCREEN_WIDTH-50,50);
                    unmute = !unmute;
                    _model->music.SetVolume(5);
                    _model->player_shoot_sound.SetVolume(6);
                    _model->enemy_death_sound.SetVolume(6);
                }
            }

        }
    }
    return result;
}

void GameView::sorting(int tab[], int taille)
{
    for (int i=0; i<taille; i++)
    {
        for (int j=i+1; j<taille; j++)
        {
            if (tab[i] < tab[j])
            {
                int tmp;                // Echanger tab[i] et tab[j]
                tmp = tab[i];
                tab[i] = tab[j];
                tab[j] = tmp;
            }
        }
    }
}

void GameView::setModel(GameModel * model)
{
    _model = model;
}



void GameView::HUD()
{
    /// SCORE ///
    _score = String("Score : " + _model->changeString(_model->getScore()));
    _score.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
    _score.SetColor(Color(128, 128, 0));
    _score.SetSize(25.f);                                     // change la taille de la police
    _score.SetPosition(170, 10);                // positionne le text

    /// VIE ///
    if(language==0)
        _life=String("Lives : "+_model->changeString(_model->getPlayer()->getLife()));
    else
        _life=String("Vies : "+_model->changeString(_model->getPlayer()->getLife()));
    _life.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
    _life.SetColor(Color(128, 128, 0));
    _life.SetSize(25.f);                                     // change la taille de la police
    _life.SetPosition(20, 10);                // positionne le text

    /// HP ///
    int x = (_model->getPlayer()->getHp()*140)/100;
    hp_bar=Shape::Rectangle(20, 50, 160, 70, Color(0, 0, 0), 0, Color(0,0,0));
    if(_model->getPlayer()->getHp()<=100 && _model->getPlayer()->getHp()>60)
        rect=Shape::Rectangle(20, 50, 20+x, 70, Color(58, 242, 75));

    if(_model->getPlayer()->getHp()<=60 && _model->getPlayer()->getHp()>40)
        rect=Shape::Rectangle(20, 50, 20+x, 70, Color(243, 214, 23));

    if(_model->getPlayer()->getHp()<=40 && _model->getPlayer()->getHp()>20)
        rect=Shape::Rectangle(20, 50, 20+x, 70, Color(244, 102, 27));

    if(_model->getPlayer()->getHp()<=20 && _model->getPlayer()->getHp()>0)
        rect=Shape::Rectangle(20, 50, 20+x, 70, Color(231, 62, 1));


    /// LEVEL //
    if(language==0)
        _level=String("Level : "+_model->changeString(_model->getLevel()));
    else
        _level=String("Niveau : "+_model->changeString(_model->getLevel()));
    _level.SetPosition(SCREEN_WIDTH-120,10);
    _level.SetFont(*_font);
    _level.SetSize(25.f);
    _level.SetColor(Color(128, 128, 0));

    /// ENNEMIS TUES ///
    if(language==0)
        _enemy_killed = String("Killed ennemies : " + _model->changeString(_model->getEnemyKilled()));
    else
        _enemy_killed = String("Ennemies tues : " + _model->changeString(_model->getEnemyKilled()));
    _enemy_killed.SetPosition(SCREEN_WIDTH-400, 10);
    _enemy_killed.SetFont(*_font);
    _enemy_killed.SetSize(25.f);
    _enemy_killed.SetColor(Color(128, 128, 0));

    /// BONUS ///
    if(language==0)
        _bonus=String("Bomb: "+_model->changeString(_model->getPlayer()->getBomb()));
    else
        _bonus=String("Bombe: "+_model->changeString(_model->getPlayer()->getBomb()));
    _bonus.SetFont(*_font);
    _bonus.SetPosition(SCREEN_WIDTH/2-100,10);
    _bonus.SetSize(25.f);
    _bonus.SetColor(Color(128, 128, 0));


    _window->Draw(_score);
    _window->Draw(_life);
    _window->Draw(hp_bar);
    _window->Draw(rect);
    _window->Draw(_level);
    _window->Draw(_enemy_killed);
    _window->Draw(_sound_muted_sprite);
    _window->Draw(_bonus);
}

void GameView::menu(bool &exit)
{
    bool quit=false;
    while(!quit)
    {
        _window->SetSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        _window->Clear();                           // Nettoie la fenetre avec une couleur (noir par defaut)
        _window->Draw(_menu_sprite);          // Affiche le sprite dans la fenetre*/

        _window->Draw(_play_button_1_sprite);
        _window->Draw(_quit_button_1_sprite);
        _window->Draw(_score_button_1_sprite);
        _window->Draw(_option_button_1_sprite);

        _text=String("OCEAN");
        _text.SetPosition(50, SCREEN_HEIGHT/2-100);
        _text.SetSize(75.f);
        _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
        _text.SetColor(Color(0, 125, 255));
        _window->Draw(_text);
        _text=String("INVADERS");
        _text.SetPosition(50, SCREEN_HEIGHT/2+75-100);
        _text.SetSize(75.f);
        _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
        _text.SetColor(Color(65, 113, 209));
        _window->Draw(_text);


        text_button();


        if(_window->IsOpened())
        {

            Event event;
            const sf::Input& Input = _window->GetInput();
            unsigned int x         = Input.GetMouseX();
            unsigned int y          = Input.GetMouseY();

            while (_window->GetEvent(event))
            {
                /// Si on appuie sur echap ou sur la croix, la fenetre se ferme
                if ((event.Type == Event::Closed) ||
                        ((event.Type == Event::KeyPressed) && (event.Key.Code == sf::Key::Escape)))
                {
                    quit=true;
                    exit=true;
                }

                /// PLAY ///
                if (((event.Type == Event::MouseButtonReleased)&& ((event.MouseButton.X >(SCREEN_WIDTH/2-60)) && (event.MouseButton.X<(SCREEN_WIDTH/2-60+200)) &&
                        (event.MouseButton.Y>(SCREEN_HEIGHT/2-240)) && (event.MouseButton.Y<(SCREEN_HEIGHT/2-240+120)) )))
                {
                    quit=true;
                }

                /// SCORE ///
                if (((event.Type == Event::MouseButtonReleased)&& ((event.MouseButton.X >(SCREEN_WIDTH/2-60)) && (event.MouseButton.X<(SCREEN_WIDTH/2-60+200)) &&
                        (event.MouseButton.Y>(SCREEN_HEIGHT/2-120)) && (event.MouseButton.Y<(SCREEN_HEIGHT/2-120+120)) )))
                {
                    viewScore();

                }
                /// OPTION ///
                if (((event.Type == Event::MouseButtonReleased)&& ((event.MouseButton.X >(SCREEN_WIDTH/2-60)) && (event.MouseButton.X<(SCREEN_WIDTH/2-60+200)) &&
                        (event.MouseButton.Y>(SCREEN_HEIGHT/2)) && (event.MouseButton.Y<(SCREEN_HEIGHT/2+120)) )))
                {
                    option();

                }

                /// QUIT ///
                if (((event.Type == Event::MouseButtonReleased)&& ((event.MouseButton.X >(SCREEN_WIDTH/2-60)) && (event.MouseButton.X<(SCREEN_WIDTH/2-60+200)) &&
                        (event.MouseButton.Y>(SCREEN_HEIGHT/2+120)) && (event.MouseButton.Y<(SCREEN_HEIGHT/2+120+120)) )))
                {
                    quit=true;
                    exit=true;
                }


            }
            if( ((x >(SCREEN_WIDTH/2-60)) && (x<(SCREEN_WIDTH/2-60+200)) &&
                    (y>(SCREEN_HEIGHT/2-240)) && (y<(SCREEN_HEIGHT/2-240+120)) ))
            {
                _window->Draw(_play_button_2_sprite);
                text_button();

            }
            if( ((x >(SCREEN_WIDTH/2-60)) && (x<(SCREEN_WIDTH/2-60+200)) &&
                    (y>(SCREEN_HEIGHT/2-120)) && (y<(SCREEN_HEIGHT/2-120+120)) ))
            {
                _window->Draw(_score_button_2_sprite);
                text_button();

            }
            if( ((x >(SCREEN_WIDTH/2-60)) && (x<(SCREEN_WIDTH/2-60+200)) &&
                    (y>(SCREEN_HEIGHT/2)) && (y<(SCREEN_HEIGHT/2+120)) ))
            {
                _window->Draw(_option_button_2_sprite);
                text_button();

            }
            if( ((x >(SCREEN_WIDTH/2-60)) && (x<(SCREEN_WIDTH/2-60+200)) &&
                    (y>(SCREEN_HEIGHT/2+120)) && (y<(SCREEN_HEIGHT/2+120+120)) ))
            {
                _window->Draw(_quit_button_2_sprite);
                text_button();

            }
        }
        _window->Display();
    }
}

void GameView::option()
{
    bool actif=false;
    while(!actif)
    {
        _window->SetSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        _window->Clear();                           // Nettoie la fenetre avec une couleur (noir par defaut)
        _window->Draw(_menu_sprite);          // Affiche le sprite dans la fenetre*/
        _quit_button_1_sprite.SetPosition(SCREEN_WIDTH-300,SCREEN_HEIGHT-300);
        _quit_button_2_sprite.SetPosition(SCREEN_WIDTH-300,SCREEN_HEIGHT-300);
        _window->Draw(_quit_button_1_sprite);

        if(getLanguage()==0)
        {
            _text=String("Return");
            _text.SetSize(30.f);
            _text.SetPosition(SCREEN_WIDTH-300+50, SCREEN_HEIGHT-260);
        }
        else
        {
            _text=String("Retour");
            _text.SetSize(22.f);
            _text.SetPosition(SCREEN_WIDTH-300+50+12, SCREEN_HEIGHT-260+5);
        }

        _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
        _text.SetColor(Color(0, 0, 0));
        _window->Draw(_text);

        _option_button_1_sprite.SetPosition(SCREEN_WIDTH/4,SCREEN_HEIGHT/4);
        _option_button_2_sprite.SetPosition(SCREEN_WIDTH/4,SCREEN_HEIGHT/4);
        _window->Draw(_option_button_1_sprite);
        _play_button_1_sprite.SetPosition(SCREEN_WIDTH/4,SCREEN_HEIGHT-300);
        _play_button_2_sprite.SetPosition(SCREEN_WIDTH/4,SCREEN_HEIGHT-300);
        _window->Draw(_play_button_1_sprite);

        if(language == 0)
            _text=String("French");
        else
            _text=String("Francais");
        _text.SetPosition(SCREEN_WIDTH/4+50,SCREEN_HEIGHT/4+40);
        _text.SetFont(*_font);
        _text.SetSize(25.f);
        _text.SetColor(Color(0, 0, 0));
        _window->Draw(_text);
        if(language==0)
            _text=String("English");
        else
            _text=String("Anglais");
        _text.SetPosition(SCREEN_WIDTH/4+50,SCREEN_HEIGHT-300+40);
        _text.SetFont(*_font);
        _text.SetSize(25.f);
        _text.SetColor(Color(0, 0, 0));
        _window->Draw(_text);



        if(_window->IsOpened())
        {

            Event event;
            const sf::Input& Input = _window->GetInput();
            unsigned int x         = Input.GetMouseX();
            unsigned int y          = Input.GetMouseY();

            while (_window->GetEvent(event))
            {
                /// Si on appuie sur echap ou sur la croix, la fenetre se ferme
                if ((event.Type == Event::Closed) ||((event.Type == Event::KeyPressed) && (event.Key.Code == sf::Key::Escape)) )
                {

                    actif=true;

                }
                if (((event.Type == Event::MouseButtonReleased)&& ((event.MouseButton.X >(SCREEN_WIDTH-300)) && (event.MouseButton.X<(SCREEN_WIDTH-300+200)) &&
                        (event.MouseButton.Y>(SCREEN_HEIGHT-300)) && (event.MouseButton.Y<(SCREEN_HEIGHT-300+120)) )))
                {
                    actif=true;
                }
                if (((event.Type == Event::MouseButtonReleased)&& ((event.MouseButton.X >(SCREEN_WIDTH/4)) && (event.MouseButton.X<(SCREEN_WIDTH/4+200)) &&
                        (event.MouseButton.Y>(SCREEN_HEIGHT/4)) && (event.MouseButton.Y<(SCREEN_HEIGHT/4+120)) )))
                {
                    setLanguage(1);
                }
                if (((event.Type == Event::MouseButtonReleased)&& ((event.MouseButton.X >(SCREEN_WIDTH/4)) && (event.MouseButton.X<(SCREEN_WIDTH/4+200)) &&
                        (event.MouseButton.Y>(SCREEN_HEIGHT-300)) && (event.MouseButton.Y<(SCREEN_HEIGHT-300+120)) )))
                {
                    setLanguage(0);
                }

            }
            if (x >(SCREEN_WIDTH/4) && x<(SCREEN_WIDTH/4+200) &&
                    y>(SCREEN_HEIGHT/4) && y<(SCREEN_HEIGHT/4+120))
            {
                _window->Draw(_option_button_2_sprite);
                if(language==0)
                    _text=String("French");
                else
                    _text=String("Francais");
                _text.SetPosition(SCREEN_WIDTH/4+50,SCREEN_HEIGHT/4+40);
                _text.SetFont(*_font);
                _text.SetSize(25.f);
                _text.SetColor(Color(0, 0, 0));
                _window->Draw(_text);
            }
            if (x >(SCREEN_WIDTH/4) && x<(SCREEN_WIDTH/4+200) &&
                    y>(SCREEN_HEIGHT-300) && y<(SCREEN_HEIGHT-300+120))
            {
                _window->Draw(_play_button_2_sprite);
                if(language==0)
                    _text=String("English");
                else
                    _text=String("Anglais");

                _text.SetPosition(SCREEN_WIDTH/4+50,SCREEN_HEIGHT-300+40);
                _text.SetFont(*_font);
                _text.SetSize(25.f);
                _text.SetColor(Color(0, 0, 0));
                _window->Draw(_text);
            }
            if( ((x >(SCREEN_WIDTH-300)) && (x<(SCREEN_WIDTH-300+200)) &&
                    (y>(SCREEN_HEIGHT-300)) && (y<(SCREEN_HEIGHT-300+120)) ))
            {
                _window->Draw(_quit_button_2_sprite);
                if(getLanguage()==0)
                {
                    _text=String("Return");
                    _text.SetSize(30.f);
                    _text.SetPosition(SCREEN_WIDTH-300+50, SCREEN_HEIGHT-260);
                }
                else
                {
                    _text=String("Retour");
                    _text.SetSize(22.f);
                    _text.SetPosition(SCREEN_WIDTH-300+50+15, SCREEN_HEIGHT-260+5);
                }
                _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
                _text.SetColor(Color(0, 0, 0));
                _window->Draw(_text);

            }
        }

        _window->Display();
    }

    //Replace correctement les images pour le menu
    _quit_button_1_sprite.SetPosition(SCREEN_WIDTH/2-60,SCREEN_HEIGHT/2+120);
    _quit_button_2_sprite.SetPosition(SCREEN_WIDTH/2-60,SCREEN_HEIGHT/2+120);
    _option_button_1_sprite.SetPosition(SCREEN_WIDTH/2-60,SCREEN_HEIGHT/2);
    _option_button_2_sprite.SetPosition(SCREEN_WIDTH/2-60,SCREEN_HEIGHT/2);
    _play_button_1_sprite.SetPosition(SCREEN_WIDTH/2-60,SCREEN_HEIGHT/2-240);
    _play_button_2_sprite.SetPosition(SCREEN_WIDTH/2-60,SCREEN_HEIGHT/2-240);
}

void GameView::pause(bool &b)
{
    bool quit=false;
    bool exit=false;
    while(!quit)
    {
        _window->Clear();
        _model->music.Stop();

        _text=String("PAUSE");
        _text.SetFont(*_font);
        _text.SetColor(Color(255, 255, 255));
        _text.SetSize(100.f);
        _text.SetPosition(SCREEN_WIDTH/3-10, SCREEN_HEIGHT/3);
        _window->Draw(_text);
        if(getLanguage()==0)
            _text=String("press p to play");
        else
            _text=String("appuyer sur p pour reprendre");
        _text.SetFont(*_font);
        _text.SetColor(Color(255, 255, 255));
        _text.SetSize(25.f);
        _text.SetPosition(SCREEN_WIDTH/3-10-50, SCREEN_HEIGHT/3+170);
        _window->Draw(_text);
        if(getLanguage()==0)
            _text=String("press m to menu");
        else
            _text=String("appuyer sur m pour acceder au menu");
        _text.SetFont(*_font);
        _text.SetColor(Color(255, 255, 255));
        _text.SetSize(25.f);
        _text.SetPosition(SCREEN_WIDTH/3-10-50, SCREEN_HEIGHT/3+210);
        _window->Draw(_text);


        // Affiche la fenetre à l'ecran
        if(_window->IsOpened())
        {
            Event event;

            while (_window->GetEvent(event))
            {
                /// Si on appuie sur echap ou sur la croix, la fenetre se ferme
                if ((event.Type == Event::KeyPressed) && (event.Key.Code == sf::Key::P))
                {
                    quit=true;
                }
                if ((event.Type == Event::KeyPressed) && (event.Key.Code == sf::Key::M))
                {


                    _window->Clear();                             // Nettoie la fenetre avec une couleur (noir par defaut)
                    _model->getPlayer()->setLife(PLAYER_LIFE);
                    _model->resetScore();
                   // _model->clock.Reset();
                    _model->resetLevel();

                    menu(exit);
                    if(!exit)
                    {
                        _model->music.Play();
                        while (treatEvents())
                        {
                            _model->nextStep();
                            draw();
                        }
                    }
                    b = false;
                    quit=true;
                }

            }

        }
        _window->Display();

    }

}


void GameView::createScore()
{
    fstream f;

    /// LECTURE DES SCORES ///
    f.open("best_scores.txt", ios::in);

    if (f.fail())
        cerr << "ouverture en lecture impossible" << endl;

    for (int i=0; i<SIZE; i++)
        _scores[i] = 0;

    int score;
    int cpt = 0;

    f >> score;

    while (!f.eof())
    {
        _scores[cpt] = score;
        f >> score;
        cpt++;
    }

    f.close();

    f.open("best_scores.txt", ios::out | ios::app);       // Ouverture en mode écriture(écrase le fichier s'il existe déjà) | filename.c_str() convertit les chaines de caractères c++ en chaines de caractères c
    if( f.fail() )                             // Si il y a une erreur, arrete le programme et affiche ...
        cerr << "ouverture en ecriture impossible" << endl;

    // Ajoute le score du joueur au tableau des meilleurs scores si il peut rentrer
    bool ok = false;
    int i=0;

    while (i < SIZE && !ok)
    {
        // Si le score est l'un des 10 meilleurs
        if (_model->getScore() > _scores[i])
        {
            // Cree une copie du tableau des scores
            int tmp_scores[SIZE];
            for (int j=0; j<SIZE; j++)
                tmp_scores[j] = _scores[j];

            // Insérer le score dans le tableau des scores
            _scores[i] = _model->getScore();

            // Décaler le tableau
            for (int j=i; j<SIZE; j++)
                _scores[j+1] = tmp_scores[j];

            // Effacer toutes les lignes du fichier
            clearFile("best_scores.txt");

            // Remplit le fichier avec les nouveaux scores
            for (int j=0; j<SIZE; j++)
                f << _scores[j] << endl;

            ok = true;
        }
        i++;
    }

    // Trie le tableau des scores
    sorting(_scores, SIZE);
    f.close();
}


void GameView::viewScore()
{

    bool actif=false;
    while(!actif)
    {
        _window->SetSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        _window->Clear();                           // Nettoie la fenetre avec une couleur (noir par defaut)
        _window->Draw(_menu_sprite);          // Affiche le sprite dans la fenetre*/
        _quit_button_1_sprite.SetPosition(SCREEN_WIDTH-300,SCREEN_HEIGHT-300);
        _quit_button_2_sprite.SetPosition(SCREEN_WIDTH-300,SCREEN_HEIGHT-300);
        _window->Draw(_quit_button_1_sprite);
        if(getLanguage()==0)
        {
            _text=String("Return");
            _text.SetSize(30.f);
            _text.SetPosition(SCREEN_WIDTH-300+50, SCREEN_HEIGHT-260);
        }
        else
        {
            _text=String("Retour");
            _text.SetSize(22.f);
            _text.SetPosition(SCREEN_WIDTH-300+50+15, SCREEN_HEIGHT-260+5);
        }

        _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
        _text.SetColor(Color(0, 0, 0));
        _window->Draw(_text);


        if(_window->IsOpened())
        {

            Event event;
            const sf::Input& Input = _window->GetInput();
            unsigned int x         = Input.GetMouseX();
            unsigned int y          = Input.GetMouseY();

            while (_window->GetEvent(event))
            {
                /// Si on appuie sur echap ou sur la croix, la fenetre se ferme
                if ((event.Type == Event::Closed) ||((event.Type == Event::KeyPressed) && (event.Key.Code == sf::Key::Escape)) )
                {

                    actif=true;

                }
                if (((event.Type == Event::MouseButtonReleased)&& ((event.MouseButton.X >(SCREEN_WIDTH-300)) && (event.MouseButton.X<(SCREEN_WIDTH-300+200)) &&
                        (event.MouseButton.Y>(SCREEN_HEIGHT-300)) && (event.MouseButton.Y<(SCREEN_HEIGHT-300+120)) )))
                {
                    actif=true;
                }

            }
            if( ((x >(SCREEN_WIDTH-300)) && (x<(SCREEN_WIDTH-300+200)) &&
                    (y>(SCREEN_HEIGHT-300)) && (y<(SCREEN_HEIGHT-300+120)) ))
            {
                _window->Draw(_quit_button_2_sprite);
                if(getLanguage()==0)
                {
                    _text=String("Return");
                    _text.SetSize(30.f);
                    _text.SetPosition(SCREEN_WIDTH-300+50, SCREEN_HEIGHT-260);
                }
                else
                {
                    _text=String("Retour");
                    _text.SetSize(22.f);
                    _text.SetPosition(SCREEN_WIDTH-300+50+15, SCREEN_HEIGHT-260+5);
                }
                _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
                _text.SetColor(Color(0, 0, 0));
                _window->Draw(_text);

            }

            createScore();
            ///affichage en tête///
            if(language==0)
                _best_score = String("Best Scores : ");     // la chaine de caractère à afficher (objet de type
            else
                _best_score = String("Meilleurs Scores : ");
            _best_score.SetPosition(SCREEN_WIDTH/2-150, 20);
            _best_score.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
            _best_score.SetColor(Color(128, 128, 0));
            _best_score.SetSize(30.f);
            _window->Draw(_best_score);
            ///affichage classement///
            int j=70;
            for(int i=1; i<SIZE+1; i++)
            {
                if(i==1)
                {
                    if(language==0)
                        _best_score=String(_model->changeString(i)+"st");
                    else
                        _best_score=String(_model->changeString(i)+"er");
                }
                else if(i==2)
                {
                    if(language==0)
                        _best_score=String(_model->changeString(i)+"nd");
                    else
                        _best_score=String(_model->changeString(i)+"eme");

                }
                else if(i==3)
                {
                    if(language==0)
                        _best_score=String(_model->changeString(i)+"rd");
                    else
                        _best_score=String(_model->changeString(i)+"eme");
                }
                else
                {
                    if(language==0)
                        _best_score=String(_model->changeString(i)+"th");
                    else
                        _best_score=String(_model->changeString(i)+"eme");
                }
                _best_score.SetPosition(SCREEN_WIDTH/2-150, j);
                j+=40;
                _best_score.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
                _best_score.SetColor(Color(128, 128, 0));
                _best_score.SetSize(30.f);
                _window->Draw(_best_score);
            }
            ///affichage score///
            j=70;
            for(int i=0; i<SIZE; i++)
            {
                _best_score = String(_model->changeString(_scores[i]));// +_model->changeString(_scores[i])+"\n");
                _best_score.SetPosition(SCREEN_WIDTH/2, j);
                j+=40;
                _best_score.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
                _best_score.SetColor(Color(128, 128, 0));
                _best_score.SetSize(30.f);
                _window->Draw(_best_score);
            }
        }
        _window->Display();
    }


    _quit_button_1_sprite.SetPosition(SCREEN_WIDTH/2-60,SCREEN_HEIGHT/2+120);
    _quit_button_2_sprite.SetPosition(SCREEN_WIDTH/2-60,SCREEN_HEIGHT/2+120);
}

void GameView::text_button()
{
    if (getLanguage()==0)
    {
        _text=String("Play");
        _text.SetPosition(SCREEN_WIDTH/2+8, SCREEN_HEIGHT/2-200);
        _text.SetSize(30.f);
    }
    else
    {
        _text=String("Jouer");
        _text.SetPosition(SCREEN_WIDTH/2+10, SCREEN_HEIGHT/2-195);
        _text.SetSize(22.f);
    }

    _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
    _text.SetColor(Color(0, 0, 0));
    _window->Draw(_text);

    if (getLanguage() == 0)
    {
        _text=String("Best Scores");
        _text.SetPosition(SCREEN_WIDTH/2-40, SCREEN_HEIGHT/2-80);
        _text.SetSize(30.f);
    }

    else
    {
        _text=String("Meilleurs Scores");
        _text.SetPosition(SCREEN_WIDTH/2-43, SCREEN_HEIGHT/2-74);
        _text.SetSize(22.f);
    }

    _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
    _text.SetColor(Color(0, 0, 0));
    _window->Draw(_text);
    _text=String("Options");

    if(getLanguage()==0)
    {
        _text.SetPosition(SCREEN_WIDTH/2-15, SCREEN_HEIGHT/2+40);
        _text.SetSize(30.f);
    }
    else
    {
        _text.SetPosition(SCREEN_WIDTH/2, SCREEN_HEIGHT/2+45);
        _text.SetSize(22.f);
    }

    _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
    _text.SetColor(Color(0, 0, 0));
    _window->Draw(_text);

    if(getLanguage()==0)
    {
        _text=String("Quit");
        _text.SetPosition(SCREEN_WIDTH/2+6, SCREEN_HEIGHT/2+160);
        _text.SetSize(30.f);
    }
    else
    {
        _text=String("Quitter");
        _text.SetPosition(SCREEN_WIDTH/2, SCREEN_HEIGHT/2+165);
        _text.SetSize(22.f);
    }


    _text.SetFont(*_font);                                    // stext sera afficher en utilisant la font _font
    _text.SetColor(Color(0, 0, 0));
    _window->Draw(_text);
}


int GameView::getLanguage()
{
    return language;
}

void GameView::setLanguage(int l)
{
    language=l;
}

void GameView::intro()
{
    _logo_sprite.Resize(150,150),
    _logo_sprite.SetPosition(0,0);
    _window->Draw(_logo_sprite);
    anim_intro.Reset();

    /** DECLENCHEUR DE CHAQUE PHASE **/
    float phase_1_time = 0;
    float phase_2_time = 1;
    float phase_3_time = 2;
    float phase_4_time = 3;
    /*********************************/
    /******** POSITION TEXTE ********/
    float pos_x = SCREEN_WIDTH/5-10;
    float pos_y = SCREEN_HEIGHT/3+10;
    /********************************/
    /******** CHANGEMENT DE PHASE ********/
    bool change_1 = false;
    bool change_2 = false;
    bool change_3 = false;
    bool change_4 = false;
    /*************************************/

    float text_size = 50.f;       // Taille du texte
    float shift = 0.1;            // Décalage
    sf::String anim;              // Texte
    bool stop_anim = false;       // Stop l'animation du texte

    anim = String("Ocean Invaders");
    anim.SetFont(*_font);
    anim.SetColor(Color(0, 0, 0));
    anim.SetSize(text_size);
    anim.SetPosition(pos_x, pos_y);

    float cpt_x = pos_x;
    float cpt_y = pos_y;

    while (!stop_anim)
    {

        if (anim_intro.GetElapsedTime() > phase_1_time && anim_intro.GetElapsedTime() < phase_2_time)
        {
            cpt_x += shift;             // Décalage du texte
            cpt_y -= shift;
            text_size += 0.01;          // Augmente la taille du texte
            anim.SetSize(text_size);    // Met à la jour la taille

            if (!change_1)
            {
                phase_2_time -= 0.1;        // Adapte la durée de la phase en fonction de la taille du texte
                shift += 0.05;              // Augmente la décalage en fonction
                change_1 = true;            // Changement de phase
                change_4 = false;
            }
        }

        if (anim_intro.GetElapsedTime() > phase_2_time && anim_intro.GetElapsedTime() < phase_3_time)
        {
            cpt_x += shift;
            cpt_y += shift;
            text_size += 0.01;
            anim.SetSize(text_size);

            if (!change_2)
            {
                phase_3_time -= 0.1;
                shift += 0.05;
                change_1 = false;
                change_2 = true;
            }
        }

        if (anim_intro.GetElapsedTime() > phase_3_time && anim_intro.GetElapsedTime() < phase_4_time)
        {
            cpt_x -= shift;
            cpt_y += shift;
            text_size += 0.01;
            anim.SetSize(text_size);

            if (!change_3)
            {
                phase_4_time -= 0.1;
                shift += 0.05;
                change_2 = false;
                change_3 = true;
            }
        }

        if (anim_intro.GetElapsedTime() > phase_4_time)
        {
            cpt_x -= shift;
            cpt_y -= shift;
            text_size += 0.01;
            anim.SetSize(text_size);

            if (!change_4)
            {
                phase_1_time -= 0.1;
                shift += 0.05;
                change_3 = false;
                change_4 = true;
            }
        }

        if (anim_intro.GetElapsedTime() > phase_4_time+1)
        {
            anim_intro.Reset();
        }

        anim.SetPosition(cpt_x, cpt_y);     // Met a jour la position du texte

        if (anim.GetSize() > 96)
        {
            stop_anim = true;
        }

        _window->Clear(Color(255,255,255));
        _window->Draw(_logo_sprite);
        _window->Draw(anim);
        _window->Display();
    }

    _window->Clear(Color(255,255,255));
    _window->Draw(_logo_sprite);
    _window->Display();
    Sleep(1);
    anim.SetSize(130);
    anim.SetPosition(30, SCREEN_HEIGHT/2-85);
    _window->Draw(anim);
    _window->Display();
    Sleep(2.5);
}
