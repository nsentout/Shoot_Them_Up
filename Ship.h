#ifndef SHIP_H
#define SHIP_H
#include <string>
#include <list>
#include "Weapon.h"
#include "MovableElement.h"

class Ship:public MovableElement
{
    protected:
        int _hp;

    public:
        Ship(int x, int y, int w, int h, int dx, int dy, int hp);
        virtual ~Ship();
        std::string toString() const;
        int getHp() const;
        void setHp(int dmg);
};

#endif
