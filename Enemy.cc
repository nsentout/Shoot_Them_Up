#include "Enemy.h"
#include <string>
#include <list>
#include <iostream>
using namespace std;

Enemy::Enemy(int x, int y, int w, int h, int dx, int dy, int hp):
    Ship(x, y, w, h, dx, dy, hp)
{}

Enemy::Enemy(Enemy *&e):
    Ship(e->getX(), e->getY(), e->getW(), e->getH(), e->getDX(), e->getDY(), e->getHp())
{}

Enemy::~Enemy()
{

}
