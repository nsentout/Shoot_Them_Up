#ifndef BONUS_H
#define BONUS_H
#include "MovableElement.h"

class Bonus:public MovableElement
{
public:
    Bonus(int x, int y, int w, int h, int dx, int dy);
    virtual ~Bonus();
    void randPositionBonus(int &x,int &y);
    int randBonus();
    void setIdentifiant(int id);

    int getId();


private:
    int _x,_y;
    int _identifiant; // -1 ->rien 0 -> bomb  1 -> shield 2 -> vie

};
const int BONUS_SIZE = 50;
const float BONUS_DELAY = 20.00;

#endif // BONUS_H
