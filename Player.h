#ifndef PLAYER_H
#define PLAYER_H
#include <string>
#include <SFML/Graphics.hpp>
#include "Weapon.h"
#include "Ship.h"
#include "MovableElement.h"

class Player:public Ship
{
protected:
    int _life;

    int have_bomb;
    int have_shield;

public:
    Player(int x, int y, int dx, int dy, int hp, int life,int bomb,int shield);
    virtual ~Player();
    std::string toString() const;
    int getLife() const;
    void setLife(int life);
    void setHp(int hp);
    int getBomb() const;
    int getShield() const;
    void setBomb(int actif);
    void setShield(int actif);
};

const int PLAYER_WIDTH = 96;
const int PLAYER_HEIGHT = 62;
const int PLAYER_HP = 100;
const int PLAYER_LIFE = 1;
const int PLAYER_X_SPEED = 2;
const int PLAYER_Y_SPEED = 4;

const int PLAYER_DMG = 50;
const float PLAYER_CADENCE = 0.2;

const int LASER_WIDTH = SCREEN_WIDTH-200;
const int LASER_HEIGHT = 20;
const int LASER_DMG = 50;
const float PLAYER_LASER_CADENCE = 0.5;

#endif
