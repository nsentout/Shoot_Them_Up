#include <iostream>
#include "GameModel.h"
#include "GameView.h"

using namespace std;
using namespace sf;

int main()
{
    srand(time(NULL));
    /***************************CREATION OBJETS*******************************/
    GameModel *model = new GameModel();
    GameView *view = new GameView(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP);
    view->setModel(model);
    /*****************************INTRO/MENU**********************************/
    bool exit=false;        // Quitter le menu
    //view->intro();          // Introduction
    view->menu(exit);       // Menu
    /********************************JEU**************************************/
    if(!exit)
    {
        model->music.Play();
        while (view->treatEvents())
        {
            model->nextStep();
            view->draw();
        }
    }
    /*************************************************************************/
    return 0;
}

