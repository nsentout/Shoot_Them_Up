Projet réalisé dans le cadre de l'obtention du DUT informatique à l'IUT 
informatique de Bordeaux.
C'est un jeu de type Shoot 'em up (https://fr.wikipedia.org/wiki/Shoot_'em_up) 
réalisé en C++ utilisant la librarie SFML.

Il a été réalisé en binôme par Maxime GUILBAULT et Nicolas SENTOUT.