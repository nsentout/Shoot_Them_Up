#include "MovableElement.h"
MovableElement::MovableElement(int x, int y, int w, int h, int dx, int dy):_x(x), _y(y), _w(w), _h(h), _dx(dx), _dy(dy) {}

MovableElement::~MovableElement() {}

int MovableElement::getX() const
{
    return _x;
}

int MovableElement::getY() const
{
    return _y;
}

int MovableElement::getDX() const
{
    return _dx;
}

int MovableElement::getDY() const
{
    return _dy;
}

int MovableElement::getH() const
{
    return _h;
}

int MovableElement::getW() const
{
    return _w;
}

void MovableElement::setX(int x)
{
    _x = x;
}

void MovableElement::setY(int y)
{
    _y = y;
}

void MovableElement::setDX(int dx)
{
    _dx = dx;
}

void MovableElement::setDy(int dy)
{
    _dy = dy;
}

void MovableElement::setW(int w)
{
    _w = w;
}

void MovableElement::setH(int h)
{
    _h = h;
}
