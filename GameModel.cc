#include "GameModel.h"
#include "Bonus.h"
#include <iostream>
#include <sstream>
using namespace std;
using namespace sf;

int GameModel::enemy_killed=0;

GameModel::GameModel()
{
    //clock.Reset();
    _player = new Player(50, SCREEN_HEIGHT/2-PLAYER_HEIGHT/2, PLAYER_X_SPEED, PLAYER_Y_SPEED, PLAYER_HP, PLAYER_LIFE, 0, 0);
    _pmissile = new Weapon(getXPlayer()+PLAYER_WIDTH, getYPlayer()+PLAYER_HEIGHT/2, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, PLAYER_DMG);
    _p_laser = new Weapon(getXPlayer()+PLAYER_WIDTH, getYPlayer()+PLAYER_HEIGHT/2, LASER_WIDTH, LASER_HEIGHT, 0, 0, LASER_DMG);
    _enemy1 = new Enemy(SCREEN_WIDTH, 0, ENEMY1_WIDTH, ENEMY1_HEIGHT, ENEMY_X_SPEED, ENEMY_Y_SPEED, ENEMY1_HP);
    _enemy2 = new Enemy(SCREEN_WIDTH, 0, ENEMY2_WIDTH, ENEMY2_HEIGHT, ENEMY_X_SPEED, ENEMY_Y_SPEED, ENEMY2_HP);
    _enemy3 = new Enemy(SCREEN_WIDTH, 0, ENEMY3_WIDTH, ENEMY3_HEIGHT, ENEMY_X_SPEED, ENEMY_Y_SPEED, ENEMY3_HP);
    _bonus = new Bonus(0,0,BONUS_SIZE,BONUS_SIZE,-1,0);

    music.OpenFromFile("sounds/music.wav");
    music.SetVolume(5);

    player_shoot_buffer.LoadFromFile("sounds/missile.wav");
    player_shoot_sound.SetBuffer(player_shoot_buffer);
    player_shoot_sound.SetVolume(6);
    laser_buffer.LoadFromFile("sounds/3532.wav");
    laser_sound.SetBuffer(laser_buffer);
    laser_sound.SetVolume(8);
    enemy_death_buffer.LoadFromFile("sounds/enemy_death.wav");
    enemy_death_sound.SetBuffer(enemy_death_buffer);
    enemy_death_sound.SetVolume(8);
}

GameModel::~GameModel()
{
    if (_player != nullptr)
        delete _player;
    if (_pmissile != nullptr)
        delete _pmissile;
    if (_p_laser != nullptr)
        delete _p_laser;
    if (_enemy1 != nullptr)
        delete _enemy1;
    if (_enemy2 != nullptr)
        delete _enemy2;
    if (_enemy3 != nullptr)
        delete _enemy3;
    if (_bonus!= nullptr)
        delete _bonus;
}

void GameModel::nextStep()
{
    movePlayerMissiles();
    addEnemy();
    moveEnemy();
    enemyShoot();
    Sleep(0.005);


    moveBonus();

}

int GameModel::getLevel() const
{
    return _level;
}

void GameModel::setLevel(int level)
{
    _level = level;
}

int GameModel::getEnemyKilled() const
{
    return enemy_killed;
}

void GameModel::resetScore()
{
    _score = 0;
}

void GameModel::resetLevel()
{
    music.Stop();
    _player->setHp(PLAYER_HP);
    _player->setX(50);
    _player->setY(SCREEN_HEIGHT/2-PLAYER_HEIGHT/2);
    enemy_killed = 0;
    _player_missiles.clear();
    _enemy_missiles.clear();
    _enemy_ships.clear();
    player_shoot_sound.Stop();
    enemy_death_sound.Stop();
    _p_laser->setDmg(_p_laser->getDmg()*1.08);
    _player->setBomb(0);
    _player->setShield(0);
    _bonus->setIdentifiant(-1);

    if (_level > 3)
    {
        _enemy3->setHp(_enemy3->getHp()*1.1);
    }

}

/********************************/
/*********** PLAYER *************/
/********************************/

template <typename A, typename B>
bool GameModel::collision(A o1, B o2) const
{
    return (o1->getX() <= (o2->getX()+o2->getW()) && (o1->getX()+o1->getW()) >= o2->getX() && o1->getY() <= (o2->getY()+o2->getH()) && (o1->getY()+o1->getH()) >= o2->getY());
}


void GameModel::moveUp()
{
    // Changer les coordonnees du joueur s'il ne dépasse pas de l'écran
    if (_player->getY() > 0)
    {
        _player->setY(getYPlayer()-_player->getDY());
        player_y_movement.Reset();
    }
}

void GameModel::moveDown()
{
    // Changer les coordonnees du joueur s'il ne dépasse pas de l'écran
    if (getYPlayer() < SCREEN_HEIGHT-PLAYER_HEIGHT)
    {
        _player->setY(getYPlayer()+_player->getDY());
        player_y_movement.Reset();
    }
}

void GameModel::moveRight()
{
    // Changer les coordonnees du joueur s'il ne dépasse pas la limite fixée
    if (getXPlayer() < SCREEN_WIDTH-400)
    {
        _player->setX(getXPlayer()+_player->getDX());
        player_x_movement.Reset();
    }
}

void GameModel::moveLeft()
{
    // Changer les coordonnees du joueur s'il ne dépasse pas de l'écran
    if (getXPlayer() > 0)
    {
        _player->setX(getXPlayer()-_player->getDX());
        player_x_movement.Reset();
    }
}


void GameModel::shoot()
{
    if (player_cadence.GetElapsedTime() > PLAYER_CADENCE && _level <= 3)
    {
        player_cadence.Reset();
        _pmissile->setX(getXPlayer()+PLAYER_WIDTH);
        _pmissile->setY(getYPlayer()+PLAYER_HEIGHT/2);
        _player_missiles.push_back(new Weapon(_pmissile));

        /// SON ///
        player_shoot_sound.Play();
    }

    if (player_cadence.GetElapsedTime() > PLAYER_CADENCE && _level > 3)
    {
        player_cadence.Reset();
        _pmissile->setX(getXPlayer()+PLAYER_WIDTH);
        _pmissile->setY(getYPlayer()+PLAYER_HEIGHT/2-5);
        _player_missiles.push_back(new Weapon(_pmissile));
        _pmissile->setY(getYPlayer()+PLAYER_HEIGHT/2+5);
        _player_missiles.push_back(new Weapon(_pmissile));

        /// SON ///
         player_shoot_sound.Play();
    }
}

void GameModel::laser(bool shoot)
{

    if (shoot)
    {
        _p_laser->setX(getXPlayer()+PLAYER_WIDTH);
        _p_laser->setY(getYPlayer()+PLAYER_HEIGHT/2);
       laser_sound.Play();

        for (auto e:_enemy_ships)
        {
            if (collision(_p_laser, e))
            {
                // Adapte le laser
                tmp_e = e;
                _p_laser->setW(LASER_WIDTH-(_p_laser->getX()-e->getX()+LASER_WIDTH));

                if (laser_delay.GetElapsedTime() > PLAYER_LASER_CADENCE)
                {
                    laser_delay.Reset();
                    e->setHp(e->getHp()-_p_laser->getDmg());    // Enlever les PV du vaisseau ennemi en fonction des dégats du laser
                }

                if (isDead(e))
                {
                    enemy_death_sound.Play();       // Jouer le son de mort
                    explosion.Reset();              // Amorcer l'explosion

                    _p_laser->setW(LASER_WIDTH);    // Le laser n'est donc plus arreté par le vaisseau, on le met à sa taille maximale
                    enemy_killed++;
                    _destroyed_enemies.push_back(e);      // Pour détruire les vaisseaux détruits par un missile
                }
            }

            // S'il n'y a plus de collisions
            else if (tmp_e != nullptr && !collision(_p_laser, tmp_e))
            {
                _p_laser->setW(LASER_WIDTH);
            }

        }

        for (auto e:_destroyed_enemies)
        {
            _enemy_ships.remove(e);
        }
    }

    else
    {
        /*_p_laser->setY(SCREEN_HEIGHT);
        _p_laser->setW(LASER_WIDTH);*/
        laser_sound.Stop();
    }
}


void GameModel::movePlayerMissiles()
{
    list <Weapon*> destroyed_missiles;     // Missiles destinés à être détruit

    for (auto m:_player_missiles)
    {
        m->setX(m->getX()+m->getDX());            // Deplace le missile

        /*********** COLLISION MISSILES / VAISSEAUX ***********/
        for (auto s:_enemy_ships)
        {
            if(collision(m,s)
               && s->getX()+5 < SCREEN_WIDTH)        // Pour ne pas  tuer les ennemis avant qu'ils soient apparus à l'écran
            {
                destroyed_missiles.push_back(m);     // Pour détruire les missiles touchant les vaisseaux ennemis
                explosion.Reset();
                _player_missiles_destroyed.push_back(m);
                s->setHp(s->getHp()-m->getDmg());    // Enlever les PV du vaisseau ennemi en fonction des dégats du missile

                // Si le vaisseau ennemi n'a plus de de vie, le détruire
                if (isDead(s))
                {
                    enemy_death_sound.Play();       // Son ennemi détruit
                    explosion.Reset();              // Amorcer l'explosion
                    enemy_killed++;
                    _destroyed_enemies.push_back(s);      // Pour détruire les vaisseaux détruits par un missile
                }

            }
        }
        /******************************************************/

        // Si le missile sort de l'écran, l'ajouter à la liste des missiles devant etre détruit
        if (m->getX() > SCREEN_WIDTH)
            destroyed_missiles.push_back(m);
    }

    for (auto e:_destroyed_enemies)
    {
        _enemy_ships.remove(e);
    }


    for (auto t:destroyed_missiles)
    {
        _player_missiles.remove(t);      // Détuit les missiles sortant de l'écran ou entrant en collision avec le joueur
    }
    destroyed_missiles.clear();

    if (explosion.GetElapsedTime() > EXPLOSION_DURATION)
        _player_missiles_destroyed.clear();
}

bool GameModel::isDead(Player *player)
{
    if (_player->getLife() == 0)
    {
        _level = 1;
        enemy_killed = 0;
        return true;
    }
    return false;
}

list<Weapon*> GameModel::getPlayerMissiles() const
{
    return _player_missiles;
}

int GameModel::getXPlayer() const
{
    return _player->getX();
}

int GameModel::getYPlayer() const
{
    return _player->getY();
}

Weapon* GameModel::getPlayerLaser() const
{
    return _p_laser;
}

Player* GameModel::getPlayer() const
{
    return _player;
}

bool GameModel::playerLostLife()
{
    if (_player->getHp() <= 0)
    {
        _player->setLife(_player->getLife()-1);
        _player->setHp(PLAYER_HP);
        _player->setX(50);
        _player->setY(SCREEN_HEIGHT/2-PLAYER_HEIGHT/2);
        enemy_killed = 0;
        _player_missiles.clear();
        _enemy_missiles.clear();
        _enemy_ships.clear();
        _player->setBomb(0);
        _player->setShield(0);
        _bonus->setIdentifiant(-1);
        //clock.Reset();          // Recommence le niveau depuis le début
        return true;
    }
    return false;
}

/*********************************/
/************ ENEMY **************/
/*********************************/

int GameModel::randomPos() const
{
    int pos=(rand()%(SCREEN_HEIGHT-ENEMY3_HEIGHT)/ENEMY3_HEIGHT);//random qui permet de tomber sur des valeurs multiples de la taille de l'ennemi

    return pos*ENEMY3_HEIGHT;
}

bool GameModel::isDead(Ship *ship)
{
    if (ship->getHp() <= 0)
    {
        _score += 100*_level;
        return true;
    }
    return false;
}

void GameModel::addEnemy()
{
    if (_enemy_ships.size() < 5 && enemy_delay.GetElapsedTime() > ENEMY_DELAY)
    {
        enemy_delay.Reset();

        switch (_level)
        {
        case 1:
            _enemy1->setY(randomPos());
            _enemy_ships.push_back(new Enemy(_enemy1));
            break;
        case 2:
            _enemy2->setY(randomPos());
            _enemy_ships.push_back(new Enemy(_enemy2));
            break;
        case 3:
            _enemy3->setY(randomPos());
            _enemy_ships.push_back(new Enemy(_enemy3));
            break;
        default:
            _enemy3->setY(randomPos());
            _enemy_ships.push_back(new Enemy(_enemy3));
            break;
        }
    }
}


void GameModel::moveEnemy()
{
    list <Enemy*> _outside_enemies;     // Ennemis destinés à être détruit

    bool crash = false;                 // L'ennemi s'est crashé sur le joueur ?

    for (auto s:_enemy_ships)
    {
        s->setX(s->getX()-s->getDX());

        // Si le vaisseau disparait de l'écran, le détruire
        if (s->getX() <= -s->getW())
            _outside_enemies.push_back(s);


        /************* COLLISION ENNEMIS / JOUEUR *************/
        if (collision(_player, s))
        {
            explosion.Reset();
            _destroyed_enemies.push_back(s);
            s->setHp(0);
            if(_player->getShield()==0)
                _player->setHp(0);
            else
            {
                _player->setShield(0);
                _score += 100*_level;
            }


            player_shoot_sound.Stop();
            enemy_death_sound.Stop();

            crash = true;               // L'ennemi s'est craché
        }
        /******************************************************/

    }

    if (!crash)
    {
        playerLostLife();
        crash = false;
    }

    for (auto e:_destroyed_enemies)
    {
        _enemy_ships.remove(e);     // Détruit les missiles entrant en collision avec le joueur
    }

    for (auto o:_outside_enemies)
    {
        _enemy_ships.remove(o);     // Détruit les ennemis sortant de l'écran
    }

    if (explosion.GetElapsedTime() > EXPLOSION_DURATION)
         _destroyed_enemies.clear();
}


void GameModel::enemyShoot()
{
    list <Weapon*> _destroyed_missiles;     // Missiles destinés à être détruit

    // Chaque vaisseau tire un missile (nb de missiles total limité à 8)
    if (enemy_cadence.GetElapsedTime() > ENEMY_CADENCE)
    {
        enemy_cadence.Reset();
        switch (_level)
        {
        case 1:
            for (auto s:_enemy_ships)
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2+10, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
            break;
        case 2:
            for (auto s:_enemy_ships)
            {
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2+5, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2-5, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
            }
            break;
        case 3:
            for (auto s:_enemy_ships)
            {
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2+10, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2-10, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
            }
            break;
        default:
            for (auto s:_enemy_ships)
            {
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2+10, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
                _enemy_missiles.push_back(new Weapon(s->getX(), s->getY()+s->getH()/2-10, WEAPON_WIDTH, WEAPON_HEIGHT, WEAPON_X_SPEED, WEAPON_Y_SPEED, ENEMY_DMG));
            }
            break;
        }
    }

    for (auto m:_enemy_missiles)
    {
        m->setX(m->getX()-m->getDX());            // Deplace le missile

        // Si le missile sort de l'écran, le détruire
        if (m->getX() <= -m->getW())
            _destroyed_missiles.push_back(m);

        // Si un missile ennemi touche le joueur
        if (collision(m,_player))
        {
            if(_player->getShield()>0)
            {
                _player->setShield(_player->getShield()-1);
                _destroyed_missiles.push_back(m);
            }
            else
            {
                _player->setHp(_player->getHp()-m->getDmg());
                _destroyed_missiles.push_back(m);
            }
        }
    }

    for (auto t:_destroyed_missiles)
    {
        _enemy_missiles.remove(t);      // Détuit les missiles sortant de l'écran ou entrant en collision avec le joueur
    }
    _destroyed_missiles.clear();
}


list<Enemy*> GameModel::getEnemies() const
{
    return _enemy_ships;
}

std::list<Weapon*> GameModel::getEnemiesMissiles() const
{
    return _enemy_missiles;
}


/*********************************/
/************ BONUS **************/
/*********************************/

void GameModel::addBonus()
{

    int tmp=_bonus->randBonus();
    _bonus->setIdentifiant(tmp);
    int x,y;
    _bonus->randPositionBonus(x,y);
    _bonus->setX(x);
    _bonus->setY(y);


}
bool GameModel::collisionBonus()
{
    if(!((_player->getY() >= (_bonus->getY())+BONUS_SIZE) || (_player->getY()+_player->getH()) <= (_bonus->getY()) || _player->getX() >= (_bonus->getX()+BONUS_SIZE) || (_player->getX()+_player->getW()) <= (_bonus->getX())))
        return true;
    return false;
}
void GameModel::moveBonus()
{
    _bonus->setX(_bonus->getX()+_bonus->getDX());
    if(_bonus->getX()+BONUS_SIZE< 0)
        _bonus->setIdentifiant(-1);

    if(bonus_cadence.GetElapsedTime() > BONUS_DELAY && _bonus->getId()==-1)
    {
        bonus_cadence.Reset();
        addBonus();
    }

    if(collision(_bonus, _player))
    {
        if(_bonus->getId()==0)
            _player->setBomb(_player->getBomb()+1);
        if(_bonus->getId()==1)
            _player->setShield(5);
        if(_bonus->getId()==2)
        {
            if (_player->getHp() <= 100 && _player->getHp() >= 70)
                _player->setHp(100);
            else
                _player->setHp(_player->getHp()+30);
        }
        _bonus->setIdentifiant(-1);

    }
}

void GameModel::bomb()
{
    int tmp=0;
    if(_player->getBomb()>0)
    {
        for(auto e:_enemy_ships)
        {
            if(e->getX()+5<SCREEN_WIDTH)
            {
                tmp+=1;
                _destroyed_enemies.push_back(e);
            }
        }
        explosion.Reset();
        _player->setBomb(_player->getBomb()-1);
    }

    enemy_killed+=tmp;
    _score += (100*_level)*tmp;
}

Bonus* GameModel::getBonus()const
{
    return _bonus;
}

std::list<Enemy*> GameModel::getDestroyedEnnemies() const
{
    return _destroyed_enemies;
}

int GameModel::getScore() const
{
    return _score;
}

std::string GameModel::changeString(int Score)
{
    std::ostringstream joueur;

    joueur << Score;

    return joueur.str();
}
