#include "Weapon.h"
#include <iostream>
using namespace std;

Weapon::Weapon(Weapon *&w):
    MovableElement(w->getX(), w->getY(), w->getW(), w->getH(), w->getDX(), w->getDY()), _dmg(w->getDmg())
{}

Weapon::Weapon(int x, int y, int w, int h, int dx, int dy, int dmg):
    MovableElement(x, y, w, h, dx, dy),_dmg(dmg)
{}

int Weapon::getDmg() const
{
    return _dmg;
}

void Weapon::setDmg(int dmg)
{
    _dmg = dmg;
}
