#include "Player.h"
#include <iostream>
#include <string>
using namespace std;
using namespace sf;

Player::Player(int x, int y, int dx, int dy, int hp, int life,int bomb,int shield):
    Ship(x, y, PLAYER_WIDTH, PLAYER_HEIGHT, dx, dy, hp), _life(life),have_bomb(bomb),have_shield(shield)
{
}

Player::~Player()
{
}

int Player::getLife() const
{
    return _life;
}

void Player::setLife(int life)
{
    _life = life;
}

void Player::setHp(int hp)
{
    _hp = hp;
}

/*Bonus* Player::getBonus() const
{
    return _bonus;
}*/

int Player::getBomb() const
{
    return have_bomb;
}

int Player::getShield() const
{
    return have_shield;
}

void Player::setBomb(int actif)
{
    have_bomb=actif;
}

void Player::setShield(int actif)
{
    have_shield=actif;
}
