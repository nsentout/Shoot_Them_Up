#ifndef WEAPON_H
#define WEAPON_H
#include <string>
#include "MovableElement.h"
class Weapon:public MovableElement
{
    protected:
        int _dmg;

    public:
        Weapon(int x, int y, int w, int h, int dx, int dy, int dmg);
        Weapon(Weapon *&w);
        std::string toString() const;
        int getDmg() const;
        void setDmg(int dmg);
        int getNum() const;
};

const int WEAPON_X_SPEED = 3;
const int WEAPON_Y_SPEED = 0;
const int WEAPON_WIDTH = 11;
const int WEAPON_HEIGHT = 5;


#endif
