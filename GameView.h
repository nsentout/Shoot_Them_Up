#ifndef GAMEVIEW_H_INCLUDED
#define GAMEVIEW_H_INCLUDED
#include <list>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <fstream>
#include "GameModel.h"

const int SIZE = 10;

class GameView
{
    private:
        int _width;
        int _height;
        static int scrolling;
        sf::RenderWindow * _window;
        sf::Clock anim_intro;


        sf::Image my_image;
        sf::Image _background_image;   // Class for loading, manipulating and saving images.
        sf::Image _bomb_image;
        sf::Image _shield_image;
        sf::Image _life_image;
        sf::Image _player_image_shield;
        sf::Image _sound_muted_image;
        sf::Image _sound_unmuted_image;
        sf::Image _logo_image;

        sf::Sprite _background_sprite; // Drawable representation of a texture, with its own transformations
        sf::Sprite _background_sprite2; // Drawable representation of a texture, with its own transformations
        sf::Sprite _player_sprite;
        sf::Sprite _missile_sprite;
        sf::Sprite _laser_sprite;
        sf::Sprite _missile_enemy_sprite;
        sf::Sprite _enemy1_sprite;
        sf::Sprite _enemy2_sprite;
        sf::Sprite _enemy3_sprite;
        sf::Sprite _bomb_sprite;
        sf::Sprite _shield_sprite;
        sf::Sprite _life_sprite;
        sf::Sprite _explosion_sprite1;
        sf::Sprite _explosion_sprite2;
        sf::Sprite _explosion_sprite3;
        sf::Sprite _explosion_sprite4;
        sf::Sprite _logo_sprite;


        sf::Sprite _player_sprite_shield;

        bool unmute;
        sf::Sprite _sound_muted_sprite;

        /*Menu*/
        sf::Image _menu_image;
        sf::Image _play_button_1_image;
        sf::Image _play_button_2_image;


        /*Menu*/
        sf::Sprite _menu_sprite;
        sf::Sprite _play_button_1_sprite;//Play
        sf::Sprite _play_button_2_sprite;//Play
        sf::Sprite _score_button_1_sprite;//Score
        sf::Sprite _score_button_2_sprite;//Score
        sf::Sprite _quit_button_1_sprite;//Quit
        sf::Sprite _quit_button_2_sprite;//Quit
        sf::Sprite _option_button_1_sprite;//Option
        sf::Sprite _option_button_2_sprite;//Option

        sf::Font * _font;
        sf::String _score;
        sf::String _life;
        sf::String _hp;
        sf::String _level;
        sf::String _enemy_killed;
        sf::String _death;
        sf::String _level_change;
        sf::String _best_score;
        sf::String _text;
        sf::String _bonus;

        sf::Shape hp_bar;
        sf::Shape rect;

        GameModel * _model;
        int _scores[SIZE];
        int language;

    public:
        GameView(int w, int h, int bpp);
        virtual ~GameView();

        void intro();
        void setModel(GameModel *model);
        void sorting(int tab[], int taille);
        void createScore();
        bool treatEvents();
        void draw();
        void HUD();
        void menu(bool &exit);
        void pause(bool &p);
        void viewScore();
        void option();
        void text_button();
        int getLanguage();
        void setLanguage(int l);
};




#endif // GAMEVIEW_H_INCLUDED


