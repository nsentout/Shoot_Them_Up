#ifndef GAMEMODEL_H_INCLUDED
#define GAMEMODEL_H_INCLUDED
#include <SFML/Audio.hpp>
#include "Player.h"
#include "Enemy.h"
#include "Bonus.h"
class GameModel
{
    private:
        Player *_player;
        Weapon *_pmissile;
        Weapon *_p_laser;
        Enemy *_enemy1;
        Enemy *_enemy2;
        Enemy *_enemy3;
        Bonus *_bonus;
        static int enemy_killed;
        std::list<Enemy*> _enemy_ships;
        std::list <Weapon*> _player_missiles;
        std::list <Weapon*> _enemy_missiles;

        int _score=0;
        int _level=1;

    public:
        GameModel();
        ~GameModel();
         //sf::Clock clock;
         sf::Clock enemy_cadence;
         sf::Clock player_cadence;
         sf::Clock enemy_delay;
         sf::Clock laser_delay;
         sf::Clock bonus_cadence;
         sf::Clock explosion;

         /// MOUVEMENTS ///
         sf::Clock player_x_movement;
         sf::Clock player_y_movement;
         sf::Clock enemies_movement;

         /// MUSIQUE ///
        sf::Music music;

         /// BRUITAGES ///
        sf::SoundBuffer player_shoot_buffer;
        sf::Sound player_shoot_sound;

        sf::SoundBuffer laser_buffer;
        sf::Sound laser_sound;

        sf::SoundBuffer enemy_death_buffer;
        sf::Sound enemy_death_sound;


        void nextStep();
        int getScore() const;
        int getLevel() const;
        void setLevel(int level);
        int getEnemyKilled() const;
        void resetLevel();
        template <typename A, typename B> bool collision(A o1, B o2) const;

        std::list <Enemy*> _destroyed_enemies;


        /// PLAYER ///
        int getXPlayer() const;
        int getYPlayer() const;
        Weapon* getPlayerLaser() const;
        std::list<Weapon*> getPlayerMissiles() const;
        std::list<Weapon*> getEnemiesMissiles() const;
        std::list<Enemy*> getEnemies() const;
        std::list<Weapon*> _player_missiles_destroyed;
        Player* getPlayer() const;
        void resetScore();

        void moveUp();
        void moveDown();
        void moveRight();
        void moveLeft();
        void shoot();
        void laser(bool test);
        void bomb();
        void movePlayerMissiles();
        bool playerLostLife();
        bool isDead(Player *player);


        /// ENNEMI ///
        int randomPos() const;
        void addEnemy();
        void moveEnemy();
        void enemyShoot();
        bool isDead(Ship *ship);
        std::string changeString(int Score);

        Enemy *tmp_e = nullptr;     // Ennemi touché par le laser

        /// BONUS ///
        void addBonus();
        Bonus* getBonus()const;
        bool collisionBonus();
        void moveBonus();

        std::list<Enemy*> getDestroyedEnnemies() const;

};


//const float LEVEL_DURATION = 5.00;
const float EXPLOSION_DURATION = 0.3;



#endif // GAMEMODEL_H_INCLUDED
