#include "Bonus.h"
#include "MovableElement.h"
#include <iostream>
using namespace std;

Bonus::Bonus(int x, int y, int w, int h, int dx, int dy):MovableElement(x,y,w,h,dx,dy),_identifiant(-1)
{
}

Bonus::~Bonus()
{
    //dtor
}

void Bonus::setIdentifiant(int id)
{
    _identifiant=id;
}

int Bonus::randBonus()
{
    return (rand()%(3));

}

void Bonus::randPositionBonus(int &x,int &y)
{
    x=SCREEN_WIDTH;
    y=(rand()%(SCREEN_HEIGHT-BONUS_SIZE));
}

int Bonus::getId()
{
    return _identifiant;
}



