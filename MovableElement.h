#ifndef MOVABLEELEMENT_H_INCLUDED
#define MOVABLEELEMENT_H_INCLUDED
//#include "screen.h"

class MovableElement
{
    protected:
        int _x;
        int _y;
        int _w;
        int _h;
        int _dx;
        int _dy;

    public:
        MovableElement(int x, int y, int w, int h, int dx, int dy);
        ~MovableElement();
        int getX() const;
        int getY() const;
        int getDX() const;
        int getDY() const;
        int getH() const;
        int getW() const;
        void setX(int x);
        void setY(int y);
        void setDX(int dx);
        void setDy(int dy);
        void setW(int w);
        void setH(int h);
        void movePlayerUp();
        void movePlayerDown();

};
const int SCREEN_WIDTH = 1000;
const int SCREEN_HEIGHT = 600;
const int SCREEN_BPP = 32;


#endif // MOVABLELEMENT_H_INCLUDED
