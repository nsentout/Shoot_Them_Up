#ifndef ENEMY_H
#define ENEMY_H
#include "MovableElement.h"
#include "Ship.h"
#include "Player.h"

class Enemy : public Ship
{
    public:
        Enemy(int x, int y, int w, int h, int dx, int dy, int hp);
        Enemy(Enemy *&e);
        virtual ~Enemy();
        int getNum() const;
};

const int ENEMY1_WIDTH = 89;
const int ENEMY1_HEIGHT = 55;
const int ENEMY1_HP = 100;

const int ENEMY2_WIDTH = 84;
const int ENEMY2_HEIGHT = 69;
const int ENEMY2_HP = 150;

const int ENEMY3_WIDTH = 91;
const int ENEMY3_HEIGHT = 60;
const int ENEMY3_HP = 200;

const int ENEMY_X_SPEED = 2;
const int ENEMY_Y_SPEED = 0;
const int ENEMY_DMG = 10;
const float ENEMY_CADENCE = 1.0;
const float ENEMY_DELAY = 1.0;

#endif // ENEMY_H
