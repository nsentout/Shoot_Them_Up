#include "Ship.h"
#include <iostream>
using namespace std;

Ship::Ship(int x, int y, int w, int h, int dx, int dy, int hp):
    MovableElement(x,y,w,h,dx,dy),_hp(hp){}

Ship::~Ship()
{
}

int Ship::getHp() const
{
    return _hp;
}

void Ship::setHp(int hp)
{
    _hp = hp;
}

string Ship::toString() const
{
    return "x : " + to_string(_x) + "\ny : " + to_string(_y) + "\n";
}

